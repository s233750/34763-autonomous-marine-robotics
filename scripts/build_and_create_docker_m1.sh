#!/bin/bash
set -e
echo "Staring the build process, this might take a while..."

# Set the aarch64 platform
# PLATFORM_FLAG="--build-arg CONDA_URL=https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-aarch64.sh"

PLATFORM_FLAG="--build-arg CONDA_URL=https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh"

 docker build -t dtu_34763_ros_image --build-arg num_jobs=2 $PLATFORM_FLAG --platform linux/amd64 -f Dockerfile .


echo "Creating container..."
docker create \
    --platform linux/amd64 \
    -p 6080:80 \
    --shm-size=2048m \
    --privileged \
    -v ${PWD}/ros_ws/src:/home/ubuntu/34763-autonomous-marine-robotics/ros_ws/src:rw \
    -v ${PWD}/Training_Sessions:/home/ubuntu/34763-autonomous-marine-robotics/Training_Sessions:rw \
    --name dtu_34763-amd64 dtu_34763_ros_image

echo "Container created! Run the 'scripts/start_container.sh' script to start the container"